const findViewPoints = require("./sam-app/mesh/mesh");

const args = process.argv.slice(2);
if (args.length < 2) {
    console.log("Usage: node mesh.js [mesh file] [number of view spots]");
    process.exit(1);
}

const meshFile = args[0];
const N = parseInt(args[1], 10);

findViewPoints(meshFile, N).then(viewPoints => {
    console.log(viewPoints);
}).catch(err => {
    console.error(err);
})

