How to run:

Either 
```node index.js [mesh file] [number of view spots]```

Or to run locally using AWS SAM set your `mesh file` and `number of view spots` values in the `sam-app/events/event.json` and run the following:

```
cd sam-app
sam build
sam local invoke --event events/event.json
```

To find all the view points, set `number of view spots` to 0

The actual implementation of the solution can be found at `sam-app/mesh/mesh.js`
