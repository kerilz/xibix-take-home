const findViewPoints = require("./mesh");

const lambdaHandler = async (event, context) => {
    const meshFile = event.meshFile;
    const numberOfViewSpots = parseInt(event.numberOfViewSpots, 10);

    const viewPoints = await findViewPoints(meshFile, numberOfViewSpots);
    console.log(viewPoints)
    return {
        statusCode: 200,
        body: JSON.stringify(viewPoints),
    };
};

module.exports = {
    lambdaHandler
};
