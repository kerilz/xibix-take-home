const fs = require("fs")

const findViewPoints = async (meshFile, N) => {
    const rawData = await readFileAsync(meshFile, "utf-8")

    const data = JSON.parse(rawData);
    const elements = data.elements;

    const nonViewPoints = new Set();
    const viewPoints = []
    const sortedValues = data.values.sort((a, b) => (b.value - a.value));
    const heightMap = createHeightMap(sortedValues)

    for (const currentValue of sortedValues) {
        if (N !== 0 && viewPoints.length === N) {
            break;
        }
        if (!nonViewPoints.has(currentValue.element_id)) {
            let neighbours = getNeighbours(currentValue.element_id, elements);
            neighbours.forEach(n => nonViewPoints.add(n))

            if (!neighbours.some(n => heightMap.get(n) > currentValue.value)) {
                viewPoints.push(currentValue.element_id);
            }
        }
    }
    return viewPoints.map(p => {
        return {element_id: p, value: heightMap.get(p)}
    })
}

const readFileAsync = (filePath, encoding) => {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, encoding, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
};

const getNeighbours = (element_id, elements) => {
    const edges = elements.find(e => e.id === element_id).nodes;
    return elements.filter(e => e.nodes.some(n => edges.includes(n)) && e.id !== element_id).map(e => e.id);
}

const createHeightMap = sortedValues => {
    const heightMap = new Map();
    sortedValues.forEach(value => {
        heightMap.set(value.element_id, value.value);
    });
    return heightMap;
};

module.exports = findViewPoints;